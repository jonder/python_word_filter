import nltk
from pattern.en import lexeme, pluralize, singularize
import re

#скачивает все покеты(раскоментировать)
#nltk.download("all") 

#определение частей речи слова 
wordtags = nltk.ConditionalFreqDist((w.lower(), t) 
           for w, t in nltk.corpus.brown.tagged_words(tagset="universal"))

#приводит слово в единственое число
wl = nltk.stem.WordNetLemmatizer()

def words_filter(text, ws):
	#убираем все спец.символы из строки 
	text = re.sub('[^A-Za-z]+', ' ', text)
	#text = re.sub(r'[^\w\s]+|[\d]+', r'',text)
	#убираем дубликаты из строки 
	text = set(text.split()) 
	#приводим слова в нижний регистр
	text = [word.lower() for word in text]
	
	#создаем словарь слов
	words = {}
	#перебираем слвоа из текста 
	for word in text:
		
		# недопускаем мусор 
		if len(word) != 1:	
			
			#если слово опознано то проходит дальше
			if list(wordtags[word])[:3] != []:
				
				#если слово существительное и не местоимение
				if "NOUN" and not "PRON" in list(wordtags[word])[:3]:
					#приводим слово в настоящие время как существительное(важно)
					word = wl.lemmatize(word, "n")
				
				#если глагол
				if "VERB" in list(wordtags[word])[:3]:
					#приводим слово в настоящие время как глагол(важно)
					word = wl.lemmatize(word, "v") 
				
				
				
				#список слов l 
				words_to_trans = []
				
				#если в слове есть тэг глагола
				if "VERB" in list(wordtags[word])[:3]:
					#перибираем все формы глагола
					for wordf in lexeme(word):
						#добавляем под меткой "VERB"
						words[wordf] = ["VERB"]
						#добавляем в список слов для перевода 
						words_to_trans.append(wordf)

				#если тега глагола в слове нету 
				elif "VERB" not in list(wordtags[word])[:3]:	
					#приводим слово в начальную форму 		
					word = singularize(word)
					
					#добавляем под меткой "OTHER"
					words[word] = ["OTHER"]
					#добавляем в список слов для перевода
					words_to_trans.append(word)
					
					#если метки подходят под условия 
					if "ADJ" and "ADV" and "PRON" and "CONJ" not in list(wordtags[word])[:3]:
						#помечаем слово во множественом числе под меткой "OTHER"
						words[pluralize(word)] = ["OTHER"]
						#добаляем слово во множественом числе в список для перевода 
						words_to_trans.append(pluralize(word))
				
				#берем слова из столбца A 
				for word in ws["A"]:
					#перебираем слова из списка слов на перевод
					for tword in words_to_trans:
						#если слово из словаря совпадает со слово из списка
						if word.value == tword:
							#берем из столбца с переводом слово
							words[tword].append(ws.cell(column = 5, row = word.row).value)

				
				#обнуляем список слов	
				words_to_trans = []
						
			#если нет то помечаем как "ДРУГОЕ"			
			else:
				words[word] = ["OTHER"]
				
	
	
				
				
	return words
