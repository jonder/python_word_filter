from filemanager import read_from_input_file, read_from_input_dictionary, write_to_xl_file
from wordsfilter import words_filter


#отлавливаем ошибку 
try:
	#если файла input.txt нету то ошибка
	f = open("input.txt", "r+")
	f.close()
	
	#если файла dictionary.xlsx нету то ошибка
	f = open("dictionary.xlsx", "r+")
	f.close()

#при ошибке выводим уведомление 
except FileNotFoundError or IOError: 
	print("Ошибка!Файл input.txt или dictionary.xlsx не найден или файл output.xlsx открыт!")
	
#если нету ошибок 
else:
	print("Читаем текст...")
	text = read_from_input_file()
	ws = read_from_input_dictionary()
	print("Текст прочитан!")

	print("Фильруем слова...")
	words = words_filter(text, ws)
	print("Слова отфильтрованы!")

	print(words)

	print("Записываем слова...")
	write_to_xl_file(words)
	print("Слова записаны!")

	print("Все!Можите идти учится!")
	print("Если интересно то слов всего:" + str(len(words)))










 




































