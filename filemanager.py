from openpyxl import Workbook, load_workbook
from openpyxl.styles import PatternFill

#возвращает текст из input.txt
def read_from_input_file():
	file = open("input.txt", "r")
	text = file.read()
	return text

#возвращает страницу из dictionary.xlsx 
def read_from_input_dictionary():
	wb = load_workbook(filename = "dictionary.xlsx")
	ws = wb["Слова"]
	
	return ws
	
#записывает слова в xlsx таблицу 
def write_to_xl_file(words):
	#создаем рабоче место
	wb = Workbook()
	#получаем активную страницу
	ws = wb.active
	
	#получаем желтый цвет
	yellowFill = PatternFill(start_color='FFFF00', end_color='FFFF00', fill_type='solid')
	
	#присваеваем значение столбцу
	column_words = 1
	row_words = 1
	column_translate = 3
	row_translate = 1
	
	#список неизвестных глаголов
	unknow_verbs = []
	
	#размечаем ширину столбцов 
	ws.column_dimensions["A"].width = 25
	ws.column_dimensions["B"].width = 1
	ws.column_dimensions["C"].width = 25
	
	#перебираем словрь
	for word, elements in words.items():
		# перебираем части речи слова 
		for tag in elements:
			
			#если пометка "VERB" то закрашиваем в желтый
			if tag == "VERB":
				#пишим в клетке слово (в этом случае присваеваем клетку в переменную)
				data = ws.cell(row = row_words, column = column_words, value = word)
				#закрашиваем клетку в желтый цвет
				data.fill = yellowFill

				
			
			#слова с пометкой "OTHER" прогсто записываем
			else:
				#пишим в клетке слово
				ws.cell(row = row_words, column = column_words, value = word)
				
			
		#выводим прямую линию в стобец от слов
		ws.cell(row = row_words, column = column_words + 1, value = "|")
		
		#если есть перевод то выводим 
		if len(elements) == 2:
			ws.cell(row = row_translate, column = column_translate, value = elements[1])
		
		#если нету то пищим что нету в словаре
		else:
			ws.cell(row = row_translate, column = column_translate, value = "Not in dictionary")
			
			if elements[0] == "VERB":
				unknow_verbs.append(word)
			
		#передвигаем слово и превод на клетку ниже 
		row_translate += 1
		row_words += 1		
		
		
	#сохраняем рабочее место в файл output.xlsx
	wb.save("output_words.xlsx")

	wb = load_workbook(filename = "dictionary.xlsx")
	ws = wb["Слова"]
	
	row = ws["A"][-1].row
	print(row)
	
	for verb in unknow_verbs:
		data = ws.cell(column = column_words, row = row, value = verb)
		data.fill = yellowFill
		
		row += 1
	
	wb.save("output_dictionary.xlsx") 
















































